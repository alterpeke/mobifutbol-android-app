package com.example.mobifutbol

import android.app.Activity
import android.app.Application
import android.content.Context
import com.example.mobifutbol.dagger.module.AppModule
import com.example.mobifutbol.dagger.module.component.DaggerIMobiFutbolAppComponent
import com.example.mobifutbol.dagger.module.component.IMobiFutbolAppComponent
import javax.inject.Inject

class MobiFutbolApplication : Application() {

    companion object{
        lateinit var component: IMobiFutbolAppComponent
        lateinit var instance: MobiFutbolApplication

        fun getContext() : Context { return instance }
    }

    override fun onCreate() {
        super.onCreate()

        val appComponent = DaggerIMobiFutbolAppComponent.builder().appModule(AppModule(this)).build()
        appComponent.inject(this)

        instance = this
    }

}