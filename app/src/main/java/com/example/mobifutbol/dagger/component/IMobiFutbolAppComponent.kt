package com.example.mobifutbol.dagger.module.component

import android.content.Context
import com.example.mobifutbol.MobiFutbolApplication
import com.example.mobifutbol.dagger.module.AppModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface IMobiFutbolAppComponent {
    fun inject(app: MobiFutbolApplication)
    fun getContext(): Context
}